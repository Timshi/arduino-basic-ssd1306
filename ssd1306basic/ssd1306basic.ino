#include <SSD1306Wire.h> // documentation: https://github.com/ThingPulse/esp8266-oled-ssd1306

SSD1306Wire display(0x3c, SDA, SCL); 

void setup() {
  display.init();
  display.flipScreenVertically();
  display.setFont(ArialMT_Plain_10);
}

void loop() {
  display.clear();
  display.drawString(0, 0, "Text");
  display.display();
}
